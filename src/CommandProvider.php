<?php

namespace Jdelon02\CivicrmComposerPlugin;

use Composer\Plugin\Capability\CommandProvider as CommandProviderCapability;
use Jdelon02\CivicrmComposerPlugin\Command\CivicrmAddMissingFilesCommand;
use Jdelon02\CivicrmComposerPlugin\Command\CivicrmCommand;
use Jdelon02\CivicrmComposerPlugin\Command\CivicrmDownloadExtensionsCommand;
use Jdelon02\CivicrmComposerPlugin\Command\CivicrmRunBowerCommand;
use Jdelon02\CivicrmComposerPlugin\Command\CivicrmSyncWebAssetsCommand;

/**
 * Provides all the commands for this plugin.
 */
class CommandProvider implements CommandProviderCapability {

  /**
   * {@inheritdoc}
   */
  public function getCommands() {
    return [
      new CivicrmCommand(),
      new CivicrmRunBowerCommand(),
      new CivicrmAddMissingFilesCommand(),
      new CivicrmDownloadExtensionsCommand(),
      new CivicrmSyncWebAssetsCommand(),
    ];
  }

}